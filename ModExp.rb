def alg1 (n,base,reverse=false)
  quotient = n
  k = 0
  answer = ''
  while quotient!= 0 do
    a_k = quotient % base
    quotient = quotient / base
    k += 1
  answer += a_k.to_s
end
  reverse  ? (answer.reverse!) : answer
end
#--------------------------------------------------#
def alg5 (base,n,mod)
  a = alg1(n,2)
  x = 1
  power = base % mod
  (0..a.length-1).each { |i|
    if a[i].to_i == 1
      x = (x * power) % mod
    end
    power = (power * power) % mod
  }
  x
end


